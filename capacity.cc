// comparing size, capacity and max_size
#include <iostream>
#include <vector>

int main ()
{
    std::cout << "Shows the difference between size, capacity, and max_size" << std::endl;
    std::cout << "I have not idea why capacity is not 100!" << std::endl;
    std::cout << "Try commenting/uncommenting two sets of code" << std::endl;
    std::cout << "I could not get both sets to work in same code" << std::endl;
    std::cout << std::endl;
    std::vector<int> myvector;


    /*
     *This one give different size and capacity
     */
    for (int i=0; i<100; i++) myvector.push_back(i);

    std::cout << "size: "     << myvector.size()     << '\n';
    std::cout << "capacity: " << myvector.capacity() << '\n';
    std::cout << "max_size: " << myvector.max_size() << '\n';

    /*
     *This one always has capacity of 100
     */
/*
 *    myvector.clear();
 *    myvector.reserve(100);
 *
 *    std::cout << "size: "     << myvector.size()     << '\n';
 *    std::cout << "capacity: " << myvector.capacity() << '\n';
 *    std::cout << "max_size: " << myvector.max_size() << '\n';
 */

    return 0;
}
