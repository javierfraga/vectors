// vector::data
#include <iostream>
#include <vector>

int main ()
{
    std::cout << "Returns a direct pointer to the memory array used internally by the vector to store its owned elements.  Because elements in the vector are guaranteed to be stored in contiguous storage locations in the same order as represented by the vector, the pointer retrieved can be offset to access any element in the array." << std::endl;
    std::cout <<  std::endl;
  std::vector<int> myvector (5);

  int* p = myvector.data();

  *p = 10;
  ++p;
  *p = 20;
  p[2] = 100;

  std::cout << "myvector contains:";
  for (unsigned i=0; i<myvector.size(); ++i)
    std::cout << ' ' << myvector[i];
  std::cout << '\n';

  return 0;
}
