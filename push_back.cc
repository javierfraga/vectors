// vector::push_back
#include <iostream>
#include <vector>

int main ()
{
    std::cout << "This just append to vector array" << std::endl;
    std::cout << std::endl;
  std::vector<int> myvector;
  int myint;

  std::cout << "Please enter some integers (enter 0 to end):\n";

  do {
    std::cin >> myint;
    myvector.push_back (myint);
  } while (myint);

  std::cout << "myvector stores " << int(myvector.size()) << " numbers.\n";
  std::cout << "and here they are" << std::endl;
  for (auto i : myvector) {
    std::cout << i << " ";
  }
  std::cout << std::endl;

  return 0;
}
